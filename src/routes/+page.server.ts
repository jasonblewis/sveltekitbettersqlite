import { getUsers,addUser } from '$lib/server/db';
import type { PageServerLoad } from './$types';
import { type Actions } from '@sveltejs/kit';

export const load = (() => {
  const users = getUsers();

  return {
    users,
  };
}) satisfies PageServerLoad;

export const actions: Actions = {
  addUser: async ({ request, locals }) => {
    console.log("got form action addUser");
    const data = await request.formData();
    console.log(data);
    
    const usernameStr = data.get('userName')?.toString();
    const nameStr = data.get('name')?.toString();
    console.log(usernameStr, nameStr);
    if (nameStr && usernameStr) {
      console.log("adding user:", nameStr, usernameStr);
      addUser(nameStr, usernameStr);
    } else {
      console.log("missing data");
    }
  },
  
};

