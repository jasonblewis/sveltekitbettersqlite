import { json } from '@sveltejs/kit';
import { db } from '$lib/server/db';
import { delUser } from '$lib/server/db/index.js';


// delete a user from the db given the id (from url parameter)
export const DELETE = async ({ request, params }) => {
  //const data = await request.json();
  // handle your DELETE data here
  console.log("inside delete request:", request,"params:", params);
  delUser(parseInt(params.id));

  return json({ status: 200});
}

