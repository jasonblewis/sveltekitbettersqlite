import { json } from '@sveltejs/kit';
import { db } from '$lib/server/db';
import { addUser } from '$lib/server/db/index.js';

// add new user to db
export const POST = async ({ request,params,url }) => {
  const data = await request.json();
  // handle your POST data here
  console.log('data',data);

  addUser(data.name, data.userName);
  return json({ message: 'POST request handled successfully',params,url });
};

