import Database from "better-sqlite3";

console.log("importing database");


const db = new Database('app.db');


console.log("create users table");

const query = `
          create table if not exists users (
            id integer primary key,
            name string not null,
            username string not null unique
          )
`;

db.exec(query);

// const data = [
//   {name: "Caleb", username: "Calcur"},
//   {name: "Brandy", username: "brand"},
//   {name: "Eli", username: "eli123"},
// ];

// const insertData = db.prepare("insert into users (name, username) values (?, ?)");

// data.forEach((user) => {
//   insertData.run(user.name, user.username);
// });




console.log("import complete");



