import Database from "better-sqlite3";
import { DB_PATH } from '$env/static/private';
import type { User } from './types';

export const db = new Database(DB_PATH, {verbose: console.log });

export function getUsers(limit = 50): User[] {
  const sql = `
  select * from users limit $limit`;
  const stmnt = db.prepare(sql);
  const rows = stmnt.all({ limit });
  return rows as User[];
};

export function addUser(nameStr: string, userName: string): void {
  const sql = `
  insert into users
  (name, username) values (?, ?)
  `;
  const stmnt = db.prepare(sql);
  if (nameStr && userName) {
    stmnt.run( nameStr, userName );
  } else {
    throw new Error('Invalid input');
  }
};

export function delUser( id: number ): void {
  const sql = `
    delete from users where id = (?)
  `;
  const stm = db.prepare(sql);
  if (id) {
    stm.run(id.toFixed(0));
  } else {
    throw new Error('invalid input id');
  };

}
